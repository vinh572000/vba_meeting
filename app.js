var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const axios = require('axios');
var sql = require('mssql');
const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const { getDate, getDuration } = require('./helper/helper');
const { CLIENT_RENEG_LIMIT } = require('tls');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//connect to sql server
const config = {
  user: 'sa',
  password: '12345a@',
  // server: '127.0.0.1', 
  server: '192.168.1.5', // or IP address
  database: 'VABMeeting',
  trustServerCertificate: true,
  options: {
    port: 1433,
    encrypt: true, // Use this if you're on Windows Azure
  },
};
//connect to sql server
sql.connect(config)
  .then(() => {
    console.log('Connected to SQL Server');
  })
  .catch((err) => {
    console.error('Error connecting to SQL Server:', err);
  });

//get date tu chuoi string
let Data_Meeting = [];

const loginUrl = 'https://lichhop.vietabank.com.vn/security/login.aspx'; // URL của trang đăng nhập
const username = 'Infiniti';
const password = 'PXas21275';

let time_params1 = getDuration(0);
let targetUrl1 = `https://lichhop.vietabank.com.vn/modules/gcal/main.aspx?gcal_action=2&GrpId=1&view=week&date=${time_params1}`;

let time_params2 = getDuration(7);
let targetUrl2 = `https://lichhop.vietabank.com.vn/modules/gcal/main.aspx?gcal_action=2&GrpId=1&view=week&date=${time_params2}`;

let time_params3 = getDuration(14);
let targetUrl3 = `https://lichhop.vietabank.com.vn/modules/gcal/main.aspx?gcal_action=2&GrpId=1&view=week&date=${time_params3}`;


// Hàm xóa dữ liệu trong bảng cụ thể
async function deleteDataMeeting() {
  try {
    await sql.connect(config);
    const result = await sql.query('DELETE FROM dbo.Meeting_calendar');
    console.log(`Deleted ${result.rowsAffected} rows at ${new Date()}`);
  } catch (err) {
    console.error('Error deleting data:', err);
  }
}

// //xoa db lan dau tien
deleteDataMeeting();
// setTimeout(() => {
//   //goi lan 1
//   (async () => {
//     const browser = await puppeteer.launch();
//     const page = await browser.newPage();

//     // Đăng nhập bằng Puppeteer
//     await page.goto(loginUrl);
//     await page.type('#loginForm_txtUsername', username);
//     await page.type('#loginForm_txtPassword', password);

//     await Promise.all([
//       page.waitForNavigation(), // Chờ đến khi trang được tải lại sau khi đăng nhập
//       page.click('input[type="submit"]')
//     ]);
//     // Lấy nội dung sau khi đã đăng nhập
//     await page.goto(targetUrl1);
//     const content = await page.content();
//     const $ = cheerio.load(content);
//     // Chọn bảng theo ID
//     const tableId = 'GCal_BgTable';
//     const table = $(`.${tableId}`);
//     // Duyệt qua từng dòng trong tbody
//     let textCellFirst = "";
//     table.find('tbody tr').each((rowIndex, row) => {
//       const rowData = [];

//       // Tìm tất cả các ô trong dòng hiện tại
//       const cells = $(row).find('td');

//       if (cells.length === 5) {
//         cells.each((cellIndex, cell) => {
//           if (cellIndex === 0 && ($(cell).text().trim().includes("Thứ") || $(cell).text().trim().includes("Hôm") || $(cell).text().trim().includes("Chủ"))) {
//             let datef = getDate($(cell).text().trim());
//             textCellFirst = datef;
//             const cellData = $(cell).text().trim().replace(/.*/, datef);
//             rowData.push(cellData);
//           }
//           else if (cellIndex === 0 && $(cell).text().trim().includes("Chiều")) {
//             const cellData = $(cell).text().trim().replace("Chiều", textCellFirst);
//             rowData.push(cellData);
//           }
//           else {
//             const cellData = $(cell).text().trim();
//             rowData.push(cellData);
//           }
//         })
//       } else {
//         rowData.push(textCellFirst);
//         cells.each((cellIndex, cell) => {
//           const cellData = $(cell).text().trim();
//           rowData.push(cellData);
//         })
//       }

//       // In dữ liệu của toàn bộ dòng
//       let temp = {
//         time_meeting: rowData[0],
//         content_meeting: rowData[1],
//         location: rowData[2],
//         user_control: rowData[3],
//         users: rowData[4],
//       }
//       // console.log(`Row ${rowIndex + 1} Data:${rowData.join(',')}`);
//       if (rowIndex !== 0) {
//         Data_Meeting.push(temp);
//       }
//     });
//     // return;
//     try {
//       const request = new sql.Request();
//       if (Data_Meeting.length > 0) {
//         Data_Meeting.forEach(async (item, index) => {
//           if (item.content_meeting.length > 0 && item.location.length > 0) {
//             const checkExists = await request.query(`SELECT * FROM dbo.Meeting_calendar where time_meeting='${item.time_meeting}' 
//               and content_meeting=N'${item.content_meeting}' `);
//             if (checkExists.recordset.length > 0) {
//               //console.log("da ton tai");
//               return;
//             } else {
//               await request.query(`Insert into dbo.Meeting_calendar (time_meeting,content_meeting,location,user_control,users)
//                 values ('${item.time_meeting}',N'${item.content_meeting}',N'${item.location}',N'${item.user_control}',N'${item.users}')`);
//             }
//           }
//         })
//       }
//     } catch (error) {
//       console.error('Error making API call:', error.message);
//       res.status(500).json({ error: 'Internal Server Error' });
//     }

//     //console.log('Data_Meeting', Data_Meeting);
//     await browser.close();
//   })();

//   //goi lan 2 
//   (async () => {
//     const browser = await puppeteer.launch();
//     const page = await browser.newPage();

//     // Đăng nhập bằng Puppeteer
//     await page.goto(loginUrl);
//     await page.type('#loginForm_txtUsername', username);
//     await page.type('#loginForm_txtPassword', password);

//     await Promise.all([
//       page.waitForNavigation(), // Chờ đến khi trang được tải lại sau khi đăng nhập
//       page.click('input[type="submit"]')
//     ]);
//     // Lấy nội dung sau khi đã đăng nhập
//     await page.goto(targetUrl2);
//     const content = await page.content();
//     const $ = cheerio.load(content);
//     // Chọn bảng theo ID
//     const tableId = 'GCal_BgTable';
//     const table = $(`.${tableId}`);
//     // Duyệt qua từng dòng trong tbody
//     let textCellFirst = "";
//     table.find('tbody tr').each((rowIndex, row) => {
//       const rowData = [];

//       // Tìm tất cả các ô trong dòng hiện tại
//       const cells = $(row).find('td');

//       if (cells.length === 5) {
//         cells.each((cellIndex, cell) => {
//           if (cellIndex === 0 && ($(cell).text().trim().includes("Thứ") || $(cell).text().trim().includes("Hôm") || $(cell).text().trim().includes("Chủ"))) {
//             let datef = getDate($(cell).text().trim());
//             textCellFirst = datef;
//             const cellData = $(cell).text().trim().replace(/.*/, datef);
//             rowData.push(cellData);
//           }
//           else if (cellIndex === 0 && $(cell).text().trim().includes("Chiều")) {
//             const cellData = $(cell).text().trim().replace("Chiều", textCellFirst);
//             rowData.push(cellData);
//           }
//           else {
//             const cellData = $(cell).text().trim();
//             rowData.push(cellData);
//           }
//         })
//       } else {
//         rowData.push(textCellFirst);
//         cells.each((cellIndex, cell) => {
//           const cellData = $(cell).text().trim();
//           rowData.push(cellData);
//         })
//       }

//       // In dữ liệu của toàn bộ dòng
//       let temp = {
//         time_meeting: rowData[0],
//         content_meeting: rowData[1],
//         location: rowData[2],
//         user_control: rowData[3],
//         users: rowData[4],
//       }
//       // console.log(`Row ${rowIndex + 1} Data:${rowData.join(',')}`);
//       if (rowIndex !== 0) {
//         Data_Meeting.push(temp);
//       }
//     });
//     // return;
//     try {
//       const request = new sql.Request();
//       if (Data_Meeting.length > 0) {
//         Data_Meeting.forEach(async (item, index) => {
//           if (item.content_meeting.length > 0 && item.location.length > 0) {
//             const checkExists = await request.query(`SELECT * FROM dbo.Meeting_calendar where time_meeting='${item.time_meeting}' and content_meeting=N'${item.content_meeting}' `);
//             if (checkExists.recordset.length > 0) {
//               // console.log("da ton tai");
//               return;
//             } else {
//               await request.query(`Insert into dbo.Meeting_calendar (time_meeting,content_meeting,location,user_control,users)
//               values ('${item.time_meeting}',N'${item.content_meeting}',N'${item.location}',N'${item.user_control}',N'${item.users}')`);
//             }
//           }
//         })
//       }
//     } catch (error) {
//       console.error('Error making API call:', error.message);
//       res.status(500).json({ error: 'Internal Server Error' });
//     }

//     //console.log('Data_Meeting', Data_Meeting);
//     await browser.close();
//   })();

//   //goi lan 3
//   (async () => {
//     const browser = await puppeteer.launch();
//     const page = await browser.newPage();

//     // Đăng nhập bằng Puppeteer
//     await page.goto(loginUrl);
//     await page.type('#loginForm_txtUsername', username);
//     await page.type('#loginForm_txtPassword', password);

//     await Promise.all([
//       page.waitForNavigation(), // Chờ đến khi trang được tải lại sau khi đăng nhập
//       page.click('input[type="submit"]')
//     ]);
//     // Lấy nội dung sau khi đã đăng nhập
//     await page.goto(targetUrl3);
//     const content = await page.content();
//     const $ = cheerio.load(content);
//     // Chọn bảng theo ID
//     const tableId = 'GCal_BgTable';
//     const table = $(`.${tableId}`);
//     // Duyệt qua từng dòng trong tbody
//     let textCellFirst = "";
//     table.find('tbody tr').each((rowIndex, row) => {
//       const rowData = [];

//       // Tìm tất cả các ô trong dòng hiện tại
//       const cells = $(row).find('td');

//       if (cells.length === 5) {
//         cells.each((cellIndex, cell) => {
//           if (cellIndex === 0 && ($(cell).text().trim().includes("Thứ") || $(cell).text().trim().includes("Hôm") || $(cell).text().trim().includes("Chủ"))) {
//             let datef = getDate($(cell).text().trim());
//             textCellFirst = datef;
//             const cellData = $(cell).text().trim().replace(/.*/, datef);
//             rowData.push(cellData);
//           }
//           else if (cellIndex === 0 && $(cell).text().trim().includes("Chiều")) {
//             const cellData = $(cell).text().trim().replace("Chiều", textCellFirst);
//             rowData.push(cellData);
//           }
//           else {
//             const cellData = $(cell).text().trim();
//             rowData.push(cellData);
//           }
//         })
//       } else {
//         rowData.push(textCellFirst);
//         cells.each((cellIndex, cell) => {
//           const cellData = $(cell).text().trim();
//           rowData.push(cellData);
//         })
//       }

//       // In dữ liệu của toàn bộ dòng
//       let temp = {
//         time_meeting: rowData[0],
//         content_meeting: rowData[1],
//         location: rowData[2],
//         user_control: rowData[3],
//         users: rowData[4],
//       }
//       // console.log(`Row ${rowIndex + 1} Data:${rowData.join(',')}`);
//       if (rowIndex !== 0) {
//         Data_Meeting.push(temp);
//       }
//     });
//     // return;
//     try {
//       const request = new sql.Request();
//       if (Data_Meeting.length > 0) {
//         Data_Meeting.forEach(async (item, index) => {
//           if (item.content_meeting.length > 0 && item.location.length > 0) {
//             const checkExists = await request.query(`SELECT * FROM dbo.Meeting_calendar where time_meeting='${item.time_meeting}' and content_meeting=N'${item.content_meeting}' `);
//             if (checkExists.recordset.length > 0) {
//               // console.log("da ton tai");
//               return;
//             } else {
//               await request.query(`Insert into dbo.Meeting_calendar (time_meeting,content_meeting,location,user_control,users)
//                   values ('${item.time_meeting}',N'${item.content_meeting}',N'${item.location}',N'${item.user_control}',N'${item.users}')`);
//             }
//           }
//         })
//       }
//     } catch (error) {
//       console.error('Error making API call:', error.message);
//       res.status(500).json({ error: 'Internal Server Error' });
//     }

//     //console.log('Data_Meeting', Data_Meeting);
//     await browser.close();
//   })();
// }, 10000)




//lặp lại mỗi ngày
//1 sau mỗi 58 phút thì xóa db 1 lần  3480000
// setInterval(async () => {
//   deleteDataMeeting();
// }, 50000)


//sau 1 tiếng gọi 3600000
setTimeout(() => {
  (async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // Đăng nhập bằng Puppeteer
    await page.goto(loginUrl);
    await page.type('#loginForm_txtUsername', username);
    await page.type('#loginForm_txtPassword', password);

    await Promise.all([
      page.waitForNavigation(), // Chờ đến khi trang được tải lại sau khi đăng nhập
      page.click('input[type="submit"]')
    ]);
    // Lấy nội dung sau khi đã đăng nhập
    await page.goto(targetUrl1);
    const content = await page.content();
    const $ = cheerio.load(content);
    // Chọn bảng theo ID
    const tableId = 'GCal_BgTable';
    const table = $(`.${tableId}`);
    // Duyệt qua từng dòng trong tbody
    let textCellFirst = "";
    table.find('tbody tr').each((rowIndex, row) => {
      const rowData = [];

      // Tìm tất cả các ô trong dòng hiện tại
      const cells = $(row).find('td');

      if (cells.length === 5) {
        cells.each((cellIndex, cell) => {
          if (cellIndex === 0 && ($(cell).text().trim().includes("Thứ") || $(cell).text().trim().includes("Hôm") || $(cell).text().trim().includes("Chủ"))) {
            let datef = getDate($(cell).text().trim());
            textCellFirst = datef;
            const cellData = $(cell).text().trim().replace(/.*/, datef);
            rowData.push(cellData);
          }
          else if (cellIndex === 0 && $(cell).text().trim().includes("Chiều")) {
            const cellData = $(cell).text().trim().replace("Chiều", textCellFirst);
            rowData.push(cellData);
          }
          else {
            const cellData = $(cell).text().trim();
            rowData.push(cellData);
          }
        })
      } else {
        rowData.push(textCellFirst);
        cells.each((cellIndex, cell) => {
          const cellData = $(cell).text().trim();
          rowData.push(cellData);
        })
      }

      // In dữ liệu của toàn bộ dòng
      let temp = {
        time_meeting: rowData[0],
        content_meeting: rowData[1],
        location: rowData[2],
        user_control: rowData[3],
        users: rowData[4],
      }
      // console.log(`Row ${rowIndex + 1} Data:${rowData.join(',')}`);
      if (rowIndex !== 0) {
        Data_Meeting.push(temp);
      }
    });
    // return;
    try {
      const request = new sql.Request();
      if (Data_Meeting.length > 0) {
        Data_Meeting.forEach(async (item, index) => {
          if (item.content_meeting.length > 0 && item.location.length > 0) {
            const checkExists = await request.query(`SELECT * FROM dbo.Meeting_calendar where time_meeting=N'${item.time_meeting}' 
            and content_meeting=N'${item.content_meeting}' and location=N'${item.location}' `);
            if (checkExists.recordset.length > 0) {
              return;
            } else {
              await request.query(`Insert into dbo.Meeting_calendar (time_meeting,content_meeting,location,user_control,users)
              values ('${item.time_meeting}',N'${item.content_meeting}',N'${item.location}',N'${item.user_control}',N'${item.users}')`);
            }
          }
        })
      }
    } catch (error) {
      console.error('Error making API call:', error.message);
      res.status(500).json({ error: 'Internal Server Error' });
    }
    await browser.close();
  })();
}, 10000)
// //sau 1 1p tiếng gọi 3660000
setTimeout(() => {
  (async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // Đăng nhập bằng Puppeteer
    await page.goto(loginUrl);
    await page.type('#loginForm_txtUsername', username);
    await page.type('#loginForm_txtPassword', password);

    await Promise.all([
      page.waitForNavigation(), // Chờ đến khi trang được tải lại sau khi đăng nhập
      page.click('input[type="submit"]')
    ]);
    // Lấy nội dung sau khi đã đăng nhập
    await page.goto(targetUrl2);
    const content = await page.content();
    const $ = cheerio.load(content);
    // Chọn bảng theo ID
    const tableId = 'GCal_BgTable';
    const table = $(`.${tableId}`);
    // Duyệt qua từng dòng trong tbody
    let textCellFirst = "";
    table.find('tbody tr').each((rowIndex, row) => {
      const rowData = [];

      // Tìm tất cả các ô trong dòng hiện tại
      const cells = $(row).find('td');

      if (cells.length === 5) {
        cells.each((cellIndex, cell) => {
          if (cellIndex === 0 && ($(cell).text().trim().includes("Thứ") || $(cell).text().trim().includes("Hôm") || $(cell).text().trim().includes("Chủ"))) {
            let datef = getDate($(cell).text().trim());
            textCellFirst = datef;
            const cellData = $(cell).text().trim().replace(/.*/, datef);
            rowData.push(cellData);
          }
          else if (cellIndex === 0 && $(cell).text().trim().includes("Chiều")) {
            const cellData = $(cell).text().trim().replace("Chiều", textCellFirst);
            rowData.push(cellData);
          }
          else {
            const cellData = $(cell).text().trim();
            rowData.push(cellData);
          }
        })
      } else {
        rowData.push(textCellFirst);
        cells.each((cellIndex, cell) => {
          const cellData = $(cell).text().trim();
          rowData.push(cellData);
        })
      }

      // In dữ liệu của toàn bộ dòng
      let temp = {
        time_meeting: rowData[0],
        content_meeting: rowData[1],
        location: rowData[2],
        user_control: rowData[3],
        users: rowData[4],
      }
      // console.log(`Row ${rowIndex + 1} Data:${rowData.join(',')}`);
      if (rowIndex !== 0) {
        Data_Meeting.push(temp);
      }
    });
    // return;
    try {
      const request = new sql.Request();
      if (Data_Meeting.length > 0) {
        Data_Meeting.forEach(async (item, index) => {
          if (item.content_meeting.length > 0 && item.location.length > 0) {
            const checkExists = await request.query(`SELECT * FROM dbo.Meeting_calendar where time_meeting=N'${item.time_meeting}' 
            and content_meeting=N'${item.content_meeting}' and location=N'${item.location}' `);
            if (checkExists.recordset.length > 0) {
              return;
            } else {
              await request.query(`Insert into dbo.Meeting_calendar (time_meeting,content_meeting,location,user_control,users)
            values ('${item.time_meeting}',N'${item.content_meeting}',N'${item.location}',N'${item.user_control}',N'${item.users}')`);
            }
          }
        })
      }
    } catch (error) {
      console.error('Error making API call:', error.message);
      res.status(500).json({ error: 'Internal Server Error' });
    }

    await browser.close();
  })();

}, 15000)

// //sau 1 2p tiếng gọi 3720000
setTimeout(() => {
  (async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // Đăng nhập bằng Puppeteer
    await page.goto(loginUrl);
    await page.type('#loginForm_txtUsername', username);
    await page.type('#loginForm_txtPassword', password);

    await Promise.all([
      page.waitForNavigation(), // Chờ đến khi trang được tải lại sau khi đăng nhập
      page.click('input[type="submit"]')
    ]);
    // Lấy nội dung sau khi đã đăng nhập
    await page.goto(targetUrl3);
    const content = await page.content();
    const $ = cheerio.load(content);
    // Chọn bảng theo ID
    const tableId = 'GCal_BgTable';
    const table = $(`.${tableId}`);
    // Duyệt qua từng dòng trong tbody
    let textCellFirst = "";
    table.find('tbody tr').each((rowIndex, row) => {
      const rowData = [];

      // Tìm tất cả các ô trong dòng hiện tại
      const cells = $(row).find('td');

      if (cells.length === 5) {
        cells.each((cellIndex, cell) => {
          if (cellIndex === 0 && ($(cell).text().trim().includes("Thứ") || $(cell).text().trim().includes("Hôm") || $(cell).text().trim().includes("Chủ"))) {
            let datef = getDate($(cell).text().trim());
            textCellFirst = datef;
            const cellData = $(cell).text().trim().replace(/.*/, datef);
            rowData.push(cellData);
          }
          else if (cellIndex === 0 && $(cell).text().trim().includes("Chiều")) {
            const cellData = $(cell).text().trim().replace("Chiều", textCellFirst);
            rowData.push(cellData);
          }
          else {
            const cellData = $(cell).text().trim();
            rowData.push(cellData);
          }
        })
      } else {
        rowData.push(textCellFirst);
        cells.each((cellIndex, cell) => {
          const cellData = $(cell).text().trim();
          rowData.push(cellData);
        })
      }

      // In dữ liệu của toàn bộ dòng
      let temp = {
        time_meeting: rowData[0],
        content_meeting: rowData[1],
        location: rowData[2],
        user_control: rowData[3],
        users: rowData[4],
      }
      // console.log(`Row ${rowIndex + 1} Data:${rowData.join(',')}`);
      if (rowIndex !== 0) {
        Data_Meeting.push(temp);
      }
    });
    // return;
    try {
      const request = new sql.Request();
      if (Data_Meeting.length > 0) {
        Data_Meeting.forEach(async (item, index) => {
          if (item.content_meeting.length > 0 && item.location.length > 0) {
            const checkExists = await request.query(`SELECT * FROM dbo.Meeting_calendar where time_meeting=N'${item.time_meeting}' 
            and content_meeting=N'${item.content_meeting}' and location=N'${item.location}' `);
            if (checkExists.recordset.length > 0) {
              return;
            } else {
              await request.query(`Insert into dbo.Meeting_calendar (time_meeting,content_meeting,location,user_control,users)
              values ('${item.time_meeting}',N'${item.content_meeting}',N'${item.location}',N'${item.user_control}',N'${item.users}')`);
            }
          }
        })
      }
    } catch (error) {
      console.error('Error making API call:', error.message);
      res.status(500).json({ error: 'Internal Server Error' });
    }

    await browser.close();
  })();
}, 20000)




module.exports = app;
