const getDuration = (duration) => {
    if (duration === 0) {
        var ngayHienTai = new Date();
        // console.log("Thời điểm hiện tại:", ngayHienTai);
        // Lấy thông tin về tháng, ngày và năm
        var thang = ngayHienTai.getMonth() + 1;
        var ngay = ngayHienTai.getDate();
        var nam = ngayHienTai.getFullYear();

        // Định dạng thời gian theo M/dd/yyyy
        var dinhDangThoiGian = thang.toString() + '/' + ngay.toString() + '/' + nam.toString();
        console.log("Thời gian theo định dạng M/dd/yyyy:", dinhDangThoiGian);
        return dinhDangThoiGian;
    } else {
        // Thời điểm hiện tại
        var ngayHienTai = new Date();
        var soNgayThem = duration;  // Thay đổi giá trị này theo ý muốn
        // Cộng thêm số ngày
        ngayHienTai.setDate(ngayHienTai.getDate() + soNgayThem);

        // In ra kết quả
        // console.log("Thời điểm hiện tại:", ngayHienTai);
        // Lấy thông tin về tháng, ngày và năm
        var thang = ngayHienTai.getMonth() + 1; // Tháng bắt đầu từ 0, nên cộng thêm 1
        var ngay = ngayHienTai.getDate();
        var nam = ngayHienTai.getFullYear();

        // Định dạng thời gian theo M/dd/yyyy
        var dinhDangThoiGian = thang.toString() + '/' + ngay.toString() + '/' + nam.toString();
        // In ra kết quả
        console.log("Thời gian theo định dạng M/dd/yyyy:", dinhDangThoiGian);
        return dinhDangThoiGian;
    }

}

const getDate = (dateString) => {
    const match = dateString.match(/(\D+)(\d{2}\/\d{2}\/\d{2})/);
    if (match) {
        const dayOfWeek = match[1].trim(); // Lấy chuỗi ngày trong tuần
        const datePart = match[2]; // Lấy chuỗi ngày tháng

        // Tách ngày, tháng và năm
        const [day, month, year] = datePart.split('/');
        // console.log("Ngày trong tuần:", day + "/" + month + "/" + year);
        let date = day + "/" + month + "/" + year;
        return date;
    } else {
        console.log("Không tìm thấy ngày tháng trong chuỗi.");
        return null;
    }
}


module.exports = {
    getDuration,
    getDate
}